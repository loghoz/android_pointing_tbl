package com.itlabil.pointingtbl.network;

import com.itlabil.pointingtbl.model.ListLocationModel;
import com.itlabil.pointingtbl.model.TambalBanModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by itlabil on 23/11/17.
 */

public interface ApiService {

    //Get data Tambal Ban
    @GET("tambalban")
    Call<ListLocationModel> getAllLocation();

    @Headers({"Accept: application/json",})
    @POST("tambalban/tambah")
    @FormUrlEncoded
    Call<TambalBanModel> tambah(@Field("nama") String nama,
                                @Field("buka") String buka,
                                @Field("kendaraan") String kendaraan,
                                @Field("alamat") String alamat,
                                @Field("lat") String lat,
                                @Field("lng") String lng);
}
