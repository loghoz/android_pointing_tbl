package com.itlabil.pointingtbl;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.itlabil.pointingtbl.model.ListLocationModel;
import com.itlabil.pointingtbl.model.LocationModel;
import com.itlabil.pointingtbl.model.TambalBanModel;
import com.itlabil.pointingtbl.network.ApiClient;
import com.itlabil.pointingtbl.network.ApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointingActivity extends AppCompatActivity  implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    EditText etDataNama, etDataBuka, etDataAlamat, etDataLat, etDataLong;
    String pilihan;

    ApiService mApiService;

    private List<LocationModel> mListMarker = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pointing);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        etDataNama = (EditText) findViewById(R.id.inputDataNama);
        etDataBuka = (EditText) findViewById(R.id.inputDataBuka);
        etDataAlamat = (EditText) findViewById(R.id.inputDataAlamat);
        etDataLat = (EditText) findViewById(R.id.inputDataLat);
        etDataLong = (EditText) findViewById(R.id.inputDataLong);

        etDataNama.setText("Tambal Ban");
        etDataBuka.setText("07.00 - 18.00");

        mApiService = ApiClient.getClient().create(ApiService.class);
    }

    /**
     * method ini dipanggil untuk check data nama tambal ban
     */
    public void checkLokasi(View view) {

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<ListLocationModel> call = apiService.getAllLocation();
        call.enqueue(new Callback<ListLocationModel>() {
            @Override
            public void onResponse(Call<ListLocationModel> call, Response<ListLocationModel> response) {
                mListMarker = response.body().getmData();

                //Nama Tambal Ban
                etDataNama.setText("Tambal Ban "+String.valueOf(mListMarker.size()+1));
                etDataBuka.setText("07.00 - 18.00");
            }

            @Override
            public void onFailure(Call<ListLocationModel> call, Throwable t) {
                Toast.makeText(PointingActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * method ini dipanggil untuk simpan data tambal ban
     */
    public void btnSimpanDataTBL(View view) {
        Call<TambalBanModel> postTambalBan = mApiService.tambah(
                etDataNama.getText().toString(),
                etDataBuka.getText().toString(),
                pilihan,
                etDataAlamat.getText().toString(),
                etDataLat.getText().toString(),
                etDataLong.getText().toString()
        );
        postTambalBan.enqueue(new Callback<TambalBanModel>() {
            @Override
            public void onResponse(Call<TambalBanModel> call, Response<TambalBanModel> response) {
                Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<TambalBanModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });

        etDataNama.setText("Tambal Ban");
        etDataBuka.setText("07.00 - 18.00");
    }

    /**
     * method ini dipanggil saat Pilih Kendaraan
     */
    public void pilihKendaraan(View view) {
        boolean pilih = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.pilihMotor:
                if (pilih)
                    pilihan = "Motor";
                break;
            case R.id.pilihMobil:
                if (pilih)
                    pilihan = "Mobil";
                break;
            case R.id.pilihMotorMobil:
                if (pilih)
                    pilihan = "Motor - Mobil";
                break;
        }
    }

    /**
     * method ini dipanggil saat peta/map siap digunakan
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Memulai Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    /**
     * method ini digunakan untuk menginisialisasi google play service
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * method ini digunakan untuk memperbarui lokasi terkini secara berkala
     */
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * method ini digunakan untuk mencari lokasi saat ini
     */
    @Override
    public void onLocationChanged(Location location) {

        //mendapatkan lokasi saat ini
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //lokasi sekarang
        LatLng lokasiSekarang = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions lokasiSekarangOption = new MarkerOptions();
        lokasiSekarangOption.position(lokasiSekarang);

        etDataLat.setText(String.valueOf(location.getLatitude()));
        etDataLong.setText(String.valueOf(location.getLongitude()));

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            etDataLat.setText(etDataLat.getText());
            etDataLong.setText(etDataLong.getText());
            etDataAlamat.setText(addresses.get(0).getAddressLine(0));
        }catch(Exception e)
        {

        }

        //perpindahan maps
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lokasiSekarang.latitude, lokasiSekarang.longitude)).zoom(16).build();

        //pergerakan maps
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * method ini digunakan permintaan inzin akses lokasi
     */
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * method ini digunakan untuk Respon Permintaan Akses Lokasi
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Izin DI terima
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    //Izin Di tolak
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

}
