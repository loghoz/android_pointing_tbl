package com.itlabil.pointingtbl;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.itlabil.pointingtbl.model.ListLocationModel;
import com.itlabil.pointingtbl.model.LocationModel;
import com.itlabil.pointingtbl.model.TambalBanModel;
import com.itlabil.pointingtbl.network.ApiClient;
import com.itlabil.pointingtbl.network.ApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private GoogleMap mMap;

    LocationManager locationManager;

    EditText etDataNama, etDataBuka, etDataAlamat, etDataLat, etDataLong;
    String pilihan;

    ApiService mApiService;

    private List<LocationModel> mListMarker = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        etDataNama = (EditText) findViewById(R.id.inputDataNama);
        etDataBuka = (EditText) findViewById(R.id.inputDataBuka);
        etDataAlamat = (EditText) findViewById(R.id.inputDataAlamat);
        etDataLat = (EditText) findViewById(R.id.inputDataLat);
        etDataLong = (EditText) findViewById(R.id.inputDataLong);

        etDataNama.setText("Tambal Ban");

        mApiService = ApiClient.getClient().create(ApiService.class);
    }

    public void pilihKendaraan(View view) {
        boolean pilih = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.pilihMotor:
                if (pilih)
                    pilihan = "Motor";
                break;
            case R.id.pilihMobil:
                if (pilih)
                    pilihan = "Mobil";
                break;
            case R.id.pilihMotorMobil:
                if (pilih)
                    pilihan = "Motor - Mobil";
                break;
        }
    }

    public void btnSimpanDataTBL(View view) {
        Call<TambalBanModel> postTambalBan = mApiService.tambah(
                etDataNama.getText().toString(),
                etDataBuka.getText().toString(),
                pilihan,
                etDataAlamat.getText().toString(),
                etDataLat.getText().toString(),
                etDataLong.getText().toString()
        );
        postTambalBan.enqueue(new Callback<TambalBanModel>() {
            @Override
            public void onResponse(Call<TambalBanModel> call, Response<TambalBanModel> response) {
                Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<TambalBanModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });

        etDataNama.setText("Tambal Ban");
        etDataBuka.setText(null);
        etDataAlamat.setText(null);
        etDataLat.setText(null);
        etDataLong.setText(null);
    }


    public void checkLokasi(View view) {

        getLocation();

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<ListLocationModel> call = apiService.getAllLocation();
        call.enqueue(new Callback<ListLocationModel>() {
            @Override
            public void onResponse(Call<ListLocationModel> call, Response<ListLocationModel> response) {
                mListMarker = response.body().getmData();

                //Nama Tambal Ban
                etDataNama.setText("Tambal Ban "+String.valueOf(mListMarker.size()+1));
                etDataBuka.setText("07.00 - 18.00");
            }

            @Override
            public void onFailure(Call<ListLocationModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        etDataLat.setText(String.valueOf(location.getLatitude()));
        etDataLong.setText(String.valueOf(location.getLongitude()));

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            etDataLat.setText(etDataLat.getText());
            etDataLong.setText(etDataLong.getText());
            etDataAlamat.setText(addresses.get(0).getAddressLine(0));
        }catch(Exception e)
        {

        }

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(MainActivity.this, "Hidupkan GPS dan Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

}
