package com.itlabil.pointingtbl.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by itlabil on 23/11/17.
 */

public class ListLocationModel {
    @SerializedName("data")
    private List<LocationModel> mData;

    public ListLocationModel(List<LocationModel> mData) {
        this.mData = mData;
    }

    public ListLocationModel() {
    }

    public List<LocationModel> getmData() {
        return mData;
    }

    public void setmData(List<LocationModel> mData) {
        this.mData = mData;
    }
}
