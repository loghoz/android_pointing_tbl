package com.itlabil.pointingtbl.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by itlabil on 24/11/17.
 */

public class TambalBanModel {
    @SerializedName("nama")
    private String tblNama;
    @SerializedName("buka")
    private String tblBuka;
    @SerializedName("kendaraan")
    private String tblKendaraan;
    @SerializedName("alamat")
    private String tblAlamat;
    @SerializedName("lat")
    private String tblLat;
    @SerializedName("lng")
    private String tblLng;

    public String getTblNama() {
        return tblNama;
    }

    public void setTblNama(String tblNama) {
        this.tblNama = tblNama;
    }

    public String getTblBuka() {
        return tblBuka;
    }

    public void setTblBuka(String tblBuka) {
        this.tblBuka = tblBuka;
    }

    public String getTblKendaraan() {
        return tblKendaraan;
    }

    public void setTblKendaraan(String tblKendaraan) {
        this.tblKendaraan = tblKendaraan;
    }

    public String getTblAlamat() {
        return tblAlamat;
    }

    public void setTblAlamat(String tblAlamat) {
        this.tblAlamat = tblAlamat;
    }

    public String getTblLat() {
        return tblLat;
    }

    public void setTblLat(String tblLat) {
        this.tblLat = tblLat;
    }

    public String getTblLng() {
        return tblLng;
    }

    public void setTblLng(String tblLng) {
        this.tblLng = tblLng;
    }
}
